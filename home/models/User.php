<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $password
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return    [
            [['name','username','password'], 'string', 'max' => 255],
            [['username'], 'unique']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'username' => 'Username',
            'password' => 'Password',
        ];
    }

//------------------------------------------------------
//פונקציות של המחלקה שחייב לממש
 public static function findIdentityByAccessToken($token, $type = null)
    {
     //   return static::findOne(['access_token' => $token]);
    }
          public static function findIdentity($id) // אם אנחנו מחוברים ורוצים לקבל עוד פרטים על היוזר
    {
        return static::findOne($id);
    }
       public function validateAuthKey($authKey) // שלוש שווה- מאמת גם את סוג המשתנה, בדיקה יותר מחמירה
    {
       // return $this->auth_key === $authKey;
    }
      public function getAuthKey()
    {
       // return $this->auth_key;
    }
//-------------------------------------------------------------------------
    public function getId()
    {
        return $this->id;
    }
    public static function findByUsername($username)
    {
        return self::findOne(['username'=>$username]); // בודקים במסד נתונים אם יש לנו יוזרניים כמו שקיבלנו
    }

   public function validatePassword($password) 
    { // פונקציה שמשווה בין הסיסמה שהמשתמש מקליד לבין הסיסמה הקיימת המערכת כאשר הסיסמה במערכת היא מוצפנת. לכן נצפין את הסיסמה שמגיעה מהמשתמש ואז נשווה לסיסמה בדטא בייס
       return \Yii::$app->security->validatePassword($password,$this->password);
     
    }
    

      public function beforeSave($insert)
     { // אם המחלקה היא אקטיב רקוד הפונקציה תפעל כל פען לפני שנשמור משהו
        if (parent::beforeSave($insert)) { // דורסים ומפעילים שובת נרצה שזה יעבוד רק אם הפונקציה המקורית פועלת
            if($this->isNewRecord)
            { //אם זה פעם ראשונה שמשתמש נכנס, איז ניו רקורד- תכונה של איי
            $this->auth_key= \Yii::$app->security->generateRandomString(); // נגדיר הוס קי על ידיי הכנסת מחרוזת אקראית לשדה
            }
            if($this->isAttributeChanged('password'))
            { // אם בעצם בוצע שינוינ בסיסמה, נועד לזה שלא נעשה האש על האש
                $this->password = \Yii::$app->security->generatePasswordHash($this->password);
            }
            return true;
        }
        return false;
    }

}
