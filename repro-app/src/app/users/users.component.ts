import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  // Variables	
  users;
  usersKeys = [];

  constructor(private service:UsersService, private router:Router) {  	
  	service.getUsers().subscribe(response=>{      
    	this.users = response.json();
    	this.usersKeys = Object.keys(this.users);
    	console.log(response.json());         
 	  });
  }

  deleteUser(key){    
    let index = this.usersKeys.indexOf(key);
    this.usersKeys.splice(index,1);
    this.service.deleteMessage(key).subscribe(
      response=> console.log(response));
  }

  updateUser(id){
    this.service.getUser(id).subscribe(response=>{
      this.users  = response.json();      
    });   
  }

  optimisticAdd(name){
    var newKey =  this. usersKeys[this.usersKeys.length-1] +1;
    var newMessageObject ={};
    newMessageObject['name'] = name;
    this.users[newKey] = newMessageObject;
    this.usersKeys = Object.keys(this.users);
  }
  pasemisticAdd(){
  this.service.getUsers().subscribe(response=>{
        this.users = response.json();
        this.usersKeys = Object.keys(this.users);      
    });      
  }  

  ngOnInit(){
    var value = localStorage.getItem('auth');
    
    if(value == 'true'){   
      this.router.navigate(['/users']);
    }else{
      this.router.navigate(['/login']);
    }
  }

}
